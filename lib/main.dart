import 'package:SuperQuizz/pages/loader.dart';
import 'package:SuperQuizz/pages/login.dart';
import 'package:SuperQuizz/pages/quizz.dart';
import 'package:SuperQuizz/pages/ready.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:SuperQuizz/pages/home.dart';
import 'models/user_model.dart';

void main() {
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    runApp(Main());
  });
}

class Main extends StatelessWidget {
  const Main({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScopedModel<UserModel>(
      model: UserModel(),
      child: MaterialApp(
        title: "Super Quizz",
        debugShowCheckedModeBanner: false,
        home: LoaderPage(),
        routes: {
          "/loader": (context) => LoaderPage(),
          "/login": (context) => LoginPage(),
          "/home": (context) => HomePage(),
        },
      ),
    );
  }
}

