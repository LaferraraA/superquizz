import 'package:SuperQuizz/models/quizz.dart';
import 'package:SuperQuizz/pages/quizz.dart';
import 'package:flutter/material.dart';

class ReadyPage extends StatefulWidget {
  int quizzToForward;

  ReadyPage({@required this.quizzToForward});

  @override
  State<StatefulWidget> createState() {
    return _ReadyPageState();
  }
}

class _ReadyPageState extends State<ReadyPage> with SingleTickerProviderStateMixin {
  AnimationController _controller;
  String text = "READY ?";
  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat();
    changeText();
  }

  Animatable<Color> background = TweenSequence<Color>(
    [
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.purple,
          end: Colors.lightBlueAccent,
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.lightBlueAccent,
          end: Colors.cyan,
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.cyan,
          end: Colors.purple,
        ),
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _controller,
        builder: (context, child) {
          return Scaffold(
            body: Container(
              color: background
                  .evaluate(AlwaysStoppedAnimation(_controller.value)),
              child: Center(child:Text(text,style: TextStyle(color: Colors.white,fontSize: 50,fontFamily: 'Roboto',fontWeight: FontWeight.bold),)),
            ),
          );
        });
  }

  void changeText(){
    Future.delayed(const Duration(seconds: 1),(){
        setState(() {
          text = "3";
        });
    }).then((next){
      Future.delayed(const Duration(seconds: 1),(){
        setState(() {
            text = "2";
        });
      }).then((next){
        Future.delayed(const Duration(seconds: 1),(){
          setState(() {
              text = "1";
          });
        }).then((next){
          Future.delayed(const Duration(seconds: 1),(){
            setState(() {
                text = "GO !";
            });
          }).then((next){
            Future.delayed(const Duration(seconds: 1),(){
              setState(() {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => QuizzScreen(
                          quizzId: Quizzs[widget.quizzToForward].id,
                        )));
              });
            });
          });
        });
      });
    });
  }
}
