import 'package:SuperQuizz/models/quizz.dart';
import 'package:SuperQuizz/pages/quizz.dart';
import 'package:flutter/material.dart';

class FelicitationPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FelicitationPageState();
  }
}

class _FelicitationPageState extends State<FelicitationPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  String text = "FELICITATIONS !";
  bool returnText = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat();
    changeText();
  }

  Animatable<Color> background = TweenSequence<Color>(
    [
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.purple,
          end: Colors.lightBlueAccent,
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.lightBlueAccent,
          end: Colors.cyan,
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.cyan,
          end: Colors.purple,
        ),
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _controller,
        builder: (context, child) {
          return Scaffold(
            body: Container(
              color: background
                  .evaluate(AlwaysStoppedAnimation(_controller.value)),
              child: Center(child: centerContent()),
            ),
          );
        });
  }

  Widget centerContent() {
    return returnText
        ? Text(

            text,
            textAlign: TextAlign.center,
            style: TextStyle(

                color: Colors.white,
                fontSize: 50,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.bold),
          )
        : Image.asset("assets/felicitation.gif");
  }

  void changeText() {
    Future.delayed(const Duration(seconds: 2), () {
      setState(() {
        returnText = false;
      });
    }).then((next) {
      Future.delayed(const Duration(seconds: 5), () {
        setState(() {
          Navigator.pushReplacementNamed(context, "/home");
        });
      });
    });
  }
}
