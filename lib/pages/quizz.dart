import 'package:SuperQuizz/models/quizz.dart';
import 'package:SuperQuizz/pages/bonus.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';

import 'felicitations.dart';
import 'login.dart';

class QuizzScreen extends StatefulWidget {
  Quizz quizz;

  QuizzScreen({@required quizzId}) {
    quizz = Quizzs.firstWhere((q) => q.id == 0);
  }

  @override
  State<StatefulWidget> createState() {
    return _QuizzScreenState();
  }
}

class _QuizzScreenState extends State<QuizzScreen> {
  int score = 0;
  int currentQuestion = 0;
  bool isResponseToValidate = false;
  int choosedAnswer;
  double percent = 0.1;
  bool mustDisable = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        bottom: PreferredSize(
            child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  LinearPercentIndicator(
                    width: MediaQuery.of(context).size.width - 50,
                    lineHeight: 25.0,
                    percent: percent,
                    backgroundColor: Colors.grey,
                    progressColor: Colors.cyan,
                    center: Text(
                      "Question ${currentQuestion + 1} / 10",
                      style: TextStyle(color: Colors.white, fontSize: 20),
                    ),
                  ),
                ]),
            preferredSize: Size.fromHeight(6.0)),
        backgroundColor: Colors.purple,
        elevation: 0,
        automaticallyImplyLeading: false,
        centerTitle: true,
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
            Padding(
                padding: EdgeInsets.only(left: 24, right: 24, bottom: 24),
                child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Colors.white, width: 4)),
                    child: Image.asset(
                      widget.quizz.questions[currentQuestion].image,
                    ))),
            Text(
              widget.quizz.questions[currentQuestion].question,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white, fontSize: 24),
            ),
            Padding(
                padding: EdgeInsets.all(24),
                child: OutlineButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  borderSide: BorderSide(color: Colors.cyan),
                  onPressed: () {
                    mustDisable ? (){} :
                    checkAndValidate(true);
                  },
                  color: Colors.white,
                  textColor: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        widget.quizz.questions[currentQuestion].a1,
                        style: TextStyle(fontSize: 14),
                      )
                    ],
                  ),
                )),
            Padding(

                padding: EdgeInsets.only(left: 24, right: 24, top: 12),
                child: OutlineButton(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15)),
                  borderSide: BorderSide(color: Colors.cyan),
                  onPressed: () {
                    mustDisable ? () {} :
                    checkAndValidate(false);
                  },

                  color: Colors.white,
                  textColor: Colors.white,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        widget.quizz.questions[currentQuestion].a2,
                        style: TextStyle(fontSize: 14),
                      )
                    ],
                  ),
                )),
          ])),
      backgroundColor: Colors.purple,
    );
  }

  void checkAndValidate(bool accept) {
    setState(() {
      if (currentQuestion < 9) {
        currentQuestion++;
        percent += 0.1;
      } else if (currentQuestion == 9) {
        setState(() {
          mustDisable = true;
        });
        Future.delayed(const Duration(seconds: 2), () {
          mustDisable = false;
          currentQuestion++;
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => BonusPage()));
        });
      } else if (currentQuestion == 10){
          if(accept){
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => FelicitationPage()));
          }else{
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => LoginPage()));
          }
      }else{}
    });
  }
}
