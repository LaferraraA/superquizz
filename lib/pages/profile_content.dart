import 'package:SuperQuizz/models/user_model.dart';
import 'package:SuperQuizz/utils/auth.dart';
import 'package:flutter/material.dart';

class ProfileContent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _ProfileContentState();
  }
}

class _ProfileContentState extends State<ProfileContent> {
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
          Center(child: Text("Fonctionnalité en cours de développement"),)
        ,
        
        Positioned(
          bottom: 10,
          left: 10,
          right: 10,
          child: new InkWell(
            onTap: () {
              Auth().signOut().then((value) {
                UserModel.of(context).clearUser();
                Navigator.pushReplacementNamed(context, "/login");
              });
            },
            child: new Container(
              //width: 100.0,

              height: 40.0,
              decoration: new BoxDecoration(
                color: Colors.white,
                border: new Border.all(color: Colors.red, width: 2.0),
                borderRadius: new BorderRadius.circular(10.0),
              ),
              child: new Center(
                child: new Text(
                  'Se déconnecter',
                  style: new TextStyle(fontSize: 15.0, color: Colors.redAccent),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }
}
