import 'package:SuperQuizz/models/quizz.dart';
import 'package:SuperQuizz/pages/quizz.dart';
import 'package:SuperQuizz/pages/ready.dart';
import 'package:SuperQuizz/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:scoped_model/scoped_model.dart';

class HomeContent extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomeContentState();
  }
}

class _HomeContentState extends State<HomeContent> {
  _HomeContentState() {
    print("Lenght:");
  }

  @override
  Widget build(BuildContext context) {
    print("Lenght = ${Quizzs.length}");
    print("image  = ${Quizzs[0].image}");
    return Column(
      children: <Widget>[
        Container(
            padding: EdgeInsets.only(top: 16),
            child: Text("Déjà ${Quizzs.length} quizz disponible !",style: TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),),
        Expanded(
            child: ListView.builder(
          itemBuilder: (context, position) {
            print("Position = $position");
            print("Item = ${Quizzs[position]}");
            return new Container(
              child: Card(
                elevation: 10,
                borderOnForeground: true,
                margin: EdgeInsets.all(20),
                child: Column(
                  children: <Widget>[
                    Image.asset(Quizzs[position].image ?? ""),
                    Container(
                        padding: EdgeInsets.all(10),
                        child: Text(
                          Quizzs[position].name ?? '',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 16),
                        )),
                    Container(
                      padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
                      child: Text(Quizzs[position].description ?? "",
                          style: TextStyle()),
                    ),
                    RaisedButton(
                      color: hexToColor("#36b37e"),
                      textColor: Colors.white,
                      splashColor: hexToColor("#57d9a3"),
                      child: Row(
                        mainAxisSize: MainAxisSize.min,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Icon(Icons.play_arrow),
                          Text("Jouer !")
                        ],
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ReadyPage(
                                  quizzToForward: Quizzs[position].id,
                                )));
                      },
                    )
                  ],
                ),
              ),
            );
          },
          itemCount: Quizzs.length,
        ))
      ],
    );
  }
}
