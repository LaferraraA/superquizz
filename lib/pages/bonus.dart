import 'package:SuperQuizz/models/quizz.dart';
import 'package:SuperQuizz/pages/quizz.dart';
import 'package:flutter/material.dart';

class BonusPage extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    return _BonusPageState();
  }
}

class _BonusPageState extends State<BonusPage>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;
  String text = "OH";
  bool returnText = true;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      duration: const Duration(seconds: 1),
      vsync: this,
    )..repeat();
    changeText();
  }

  Animatable<Color> background = TweenSequence<Color>(
    [
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.purple,
          end: Colors.lightBlueAccent,
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.lightBlueAccent,
          end: Colors.cyan,
        ),
      ),
      TweenSequenceItem(
        weight: 1.0,
        tween: ColorTween(
          begin: Colors.cyan,
          end: Colors.purple,
        ),
      ),
    ],
  );

  @override
  Widget build(BuildContext context) {
    return AnimatedBuilder(
        animation: _controller,
        builder: (context, child) {
          return Scaffold(
            body: Container(
              color: background
                  .evaluate(AlwaysStoppedAnimation(_controller.value)),
              child: Center(child: centerContent()),
            ),
          );
        });
  }

  Widget centerContent() {
    return returnText
        ? Text(
            text,
            style: TextStyle(
                color: Colors.white,
                fontSize: 50,
                fontFamily: 'Roboto',
                fontWeight: FontWeight.bold),
          )
        : Image.asset("assets/bonus.gif");
  }

  void changeText() {
    Future.delayed(const Duration(seconds: 1), () {
      setState(() {
        text = "ATTENDS !";
      });
    }).then((next) {
      Future.delayed(const Duration(seconds: 1), () {
        setState(() {
          returnText = false;
        });
      }).then((next) {
        Future.delayed(const Duration(seconds: 3), () {
          setState(() {
            Navigator.pop(context);
          });
        });
      });
    });
  }
}
