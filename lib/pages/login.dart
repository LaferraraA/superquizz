import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:SuperQuizz/models/user_model.dart';
import 'package:SuperQuizz/utils/auth.dart';
import 'package:SuperQuizz/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:scoped_model/scoped_model.dart';

class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

enum FormMode { LOGIN, SIGNUP }

class _LoginPageState extends State<LoginPage> {
  bool _isLoading = false;
  bool _isIos;
  String _email;
  String _password;
  FormMode _formMode = FormMode.LOGIN;
  final _formKey = new GlobalKey<FormState>();

  String _errorMessage = "";

  Auth authenticator = Auth();

  @override
  void initState() {
    super.initState();
    checkUserAlreadyLoggedIn();
  }

  @override
  Widget build(BuildContext context) {
    _isIos = Theme.of(context).platform == TargetPlatform.iOS;

    return Scaffold(
      backgroundColor: hexToColor("#403294"),
      body: Stack(
        // global column
        children: <Widget>[
          _showBody(),
          _showCircularProgress(),
        ],
      ),
    );
  }

  checkUserAlreadyLoggedIn() async {
    var user = await authenticator.getCurrentUser();
    if (user != null) {
      UserModel.of(context).user = user;
      UserModel.of(context).initUser(user);
      Navigator.pushReplacementNamed(context, "/home");
    }
  }

  _showBody() {
    return Center(
        child: Container(
      padding: EdgeInsets.only(right: 16.0, left: 16.0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Stack(
                children: <Widget>[
                  Image.asset(
                    "assets/logo_bg.png",
                    height: 150,
                  ),
                  Positioned(
                    bottom: 30,
                    left: 35,
                    child: Container(
                      child: Column(
                        children: <Widget>[
                          ColorizeAnimatedTextKit(
                              onTap: () {
                                print("Tap Event");
                              },
                              text: [
                                "Super\nQuizz",
                                "Super\nQuizz",
                                "Super\nQuizz",
                              ],
                              textStyle: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 32.0,
                                  fontFamily: "Roboto"
                              ),
                              colors: [
                                hexToColor("#57d9a3"),
                                Colors.cyanAccent,
                                Colors.purpleAccent,
                                Colors.deepPurple,
                              ],
                              duration: Duration(seconds: 10),
                              textAlign: TextAlign.start,
                              alignment: AlignmentDirectional.topCenter // or Alignment.topLeft
                          )
                        ],
                      ),
                    ),
                  )
                ],
              )
            ],
          ),
          Card(
              margin: EdgeInsets.only(top: 20),
              child: Container(
                  padding: EdgeInsets.all(16),
                  child: Form(
                    key: _formKey,
                    child: ListView(
                      shrinkWrap: true,
                      children: <Widget>[
                        _showEmailInput(),
                        _showPassewordInput(),
                        _showPrimaryButton(),
                        _showSecondaryButton(),
                        _showErrorMessage()
                      ],
                    ),
                  )))
        ],
      ),
    ));
  }

  Widget _showEmailInput() {
    return Container(
      child: TextFormField(
        maxLines: 1,
        keyboardType: TextInputType.emailAddress,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'E-mail',
            icon: Icon(
              FontAwesomeIcons.envelope,
              size: 30,
              color: hexToColor("#57d9a3"),
            )),
        validator: (value) {
          return value.isEmpty ? 'Email cannot be empty :(' : null;
        },
        onSaved: (value) {
          _email = value;
        },
      ),
    );
  }

  Widget _showPassewordInput() {
    return Padding(
      padding: EdgeInsets.only(top: 50),
      child: TextFormField(
        maxLines: 1,
        obscureText: true,
        keyboardType: TextInputType.text,
        autofocus: false,
        decoration: InputDecoration(
            hintText: 'Mot de passe',
            icon: Icon(
              FontAwesomeIcons.lock,
              size: 30,
              color: hexToColor("#57d9a3"),
            )),
        validator: (value) {
          return value.isEmpty || value.length < 6
              ? 'Password must be at least 6 characters'
              : null;
        },
        onSaved: (value) {
          _password = value;
        },
      ),
    );
  }

  Widget _showPrimaryButton() {
    return new Padding(
        padding: EdgeInsets.fromLTRB(0.0, 45.0, 0.0, 0.0),
        child: MaterialButton(
          elevation: 5.0,
          minWidth: 200.0,
          height: 42.0,
          color: hexToColor("#403294"),
          child: _formMode == FormMode.LOGIN
              ? Text('Se connecter',
                  style: new TextStyle(fontSize: 20.0, color: Colors.white))
              : Text('Valider',
                  style: TextStyle(fontSize: 20.0, color: Colors.white)),
          onPressed: _validateAndSubmit,
        ));
  }

  _validateAndSubmit() async {
    setState(() {
      _errorMessage = "";
      _isLoading = true;
    });
    if (_validateAndSave()) {
      String userID = "";
      try {
        if (_formMode == FormMode.LOGIN) {
          print("FORM = LOGIN, validation credentials");
          bool isAuthenticatedAndValide =
              await authenticator.signInValidateUser(_email, _password);
          if (isAuthenticatedAndValide) {
            print("FORM = LOGIN, user isAuthenticatedAndValide ok");
            print("DEBUG user from Auth = ${authenticator.firebaseUser.email}");
            ScopedModel.of<UserModel>(context).user =
                authenticator.firebaseUser;
            UserModel.of(context).initUser(authenticator.firebaseUser);
            Navigator.pushReplacementNamed(context, "/home");
          } else {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return AlertDialog(
                    content: new Text(
                        "Il semblerait que votre adresse email ne soit pas validée.\nRegardez vos emails :-)"),
                  );
                });
          }
        } else {
          print("FORM = SIGNUP");
          userID = await authenticator.signUp(_email, _password);
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  content: Text(
                      "Un mail de vérification a été envoyé à l'adresse $_email.\nVeuillez confirmer votre adresse."),
                );
              });
          print("userID $userID");
          _changeFormToLogin();
        }
        setState(() {
          _isLoading = false;
        });
      } catch (e) {
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
        });
      }
    } else {
      setState(() {
        _isLoading = false;
      });
    }
  }

  bool _validateAndSave() {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      return true;
    }
    return false;
  }

  Widget _showSecondaryButton() {
    return FlatButton(
        child: _formMode == FormMode.LOGIN
            ? Text(
                'Créer un compte',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300),
              )
            : Text(
                'Se connecter',
                style: TextStyle(fontSize: 18.0, fontWeight: FontWeight.w300),
              ),
        onPressed: _formMode == FormMode.LOGIN
            ? _changeFormToSignUp
            : _changeFormToLogin);
  }

  _changeFormToSignUp() {
    print("_changeFormToSignUp");
    if (_formKey.currentState != null) {
      _formKey.currentState.reset();
    }
    _errorMessage = "";
    setState(() {
      _formMode = FormMode.SIGNUP;
    });
  }

  _changeFormToLogin() {
    print("_changeFormToLogin");
    if (_formKey.currentState != null) {
      _formKey.currentState.reset();
    }
    _errorMessage = "";
    setState(() {
      _formMode = FormMode.LOGIN;
    });
  }

  Widget _showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return new Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return new Container(
        height: 0.0,
      );
    }
  }

  _showCircularProgress() {
    if (_isLoading) {
      return Center(
        child: CircularProgressIndicator(
          backgroundColor: Colors.blueGrey,
        ),
      );
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }
}
