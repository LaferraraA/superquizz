import 'package:SuperQuizz/utils/functions.dart';
import 'package:flutter/material.dart';
import 'package:animated_text_kit/animated_text_kit.dart';
class LoaderPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoaderPageState();
  }
}

class _LoaderPageState extends State<LoaderPage> {
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 10),(){
    Navigator.pushReplacementNamed(context, "/login");
    });

  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/background.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Center(
          child: Transform.rotate(
            angle: 6.3,
            child: SizedBox(
              height: 200,
              child: ColorizeAnimatedTextKit(
                  onTap: () {
                    print("Tap Event");
                  },
                  text: [
                    "Super\nQuizz",
                    "Super\nQuizz",
                    "Super\nQuizz",
                  ],
                  textStyle: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 70.0,
                      fontFamily: "Roboto"
                  ),
                  colors: [
                    Colors.cyan,
                    Colors.cyanAccent,
                    Colors.purpleAccent,
                    Colors.deepPurple,
                  ],
                  duration: Duration(seconds: 5),
                  textAlign: TextAlign.start,
                  alignment: AlignmentDirectional.topCenter // or Alignment.topLeft
              ),
            ),
          ),
        )
      ],
    ));
  }
}
