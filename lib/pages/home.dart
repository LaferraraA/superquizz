import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'achivements_content.dart';
import 'home_content.dart';
import 'profile_content.dart';

class HomePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}

enum Pages { HOME, HISTORY, PROFILE }

class _HomePageState extends State<HomePage> {
  List<Widget> _pages = [HomeContent(), AchivementsContent(), ProfileContent()];
  List<Color> _colors = [Colors.deepPurple, Colors.orange, Colors.blue];
  List<String> _titles = [
    "Les quizz's",
    "Mes succès",
    "Mon profil"
  ];
  Pages _currentPage = Pages.HOME;
  int _currentIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: _showAppBar(),
      body: _showBody(),
      bottomNavigationBar: _showBottomNavigationBar(),
    );
  }

  Widget _showAppBar() {
    return AppBar(
      centerTitle: false,
      title: Container(
        margin: EdgeInsets.only(left: 10),
          child: Text(
        _titles[_currentIndex],
        style: TextStyle(fontWeight: FontWeight.bold, fontSize: 25),
      )),
      backgroundColor: _colors[_currentIndex],
    );
  }

  Widget _showBody() {
    return _pages[_currentIndex];
  }

  Widget _showBottomNavigationBar() {
    return BottomNavigationBar(
      currentIndex: _currentIndex,
      type: BottomNavigationBarType.shifting,
      items: [
        BottomNavigationBarItem(
          icon: new Icon(FontAwesomeIcons.question),
          title: Text("Quizz's"),
          backgroundColor: _colors[_currentIndex],
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.trophy),
          title: Text("Succès"),
          backgroundColor: _colors[_currentIndex],
        ),
        BottomNavigationBarItem(
          icon: Icon(FontAwesomeIcons.user),
          title: Text("Profil"),
          backgroundColor: _colors[_currentIndex],
        ),
      ],
      onTap: ((index) {
        setState(() {
          _currentIndex = index;
        });
      }),
    );
  }
}
