import 'dart:core';

import 'package:flutter/material.dart';

final List<Quizz> Quizzs = [
  new Quizz(
      0,
      "Les questions de l'impossible !",
      "Un quizz pour lequel vous n'êtes pas prêt ! Niveau de malaise 5 étoiles ...",
      "assets/impossible.gif", [
    new Question(
        0,
        "Tu préfèrerais :",
        "assets/pipi.gif",
        "Être privé d'eau à vie",
        "Devoir faire pipi toutes les 5 minutes"
        ),
    new Question(
        1,
        "Tu préfèrerais :",
        "assets/nager.gif",
        "Marcher au milieu des tigres",
        "Nager au milieu des requins"
    ),
    new Question(
        2,
        "Tu préfèrerais :",
        "assets/manger.gif",
        "Ne plus manger de viande",
        "Ne plus manger que du poisson"
    ),
    new Question(
        3,
        "Tu préfèrerais :",
        "assets/mourir.gif",
        "Que ton ou ta meilleur ami(e) meurt",
        "Que ton animal de compagnie meurt"
    ),
    new Question(
        4,
        "Tu préfèrerais :",
        "assets/bain.gif",
        "Prendre un bain à 50 degrés",
        "Prendre un bain à 5 degrés"
    ),
    new Question(
        5,
        "Tu préfèrerais :",
        "assets/electricite.gif",
        "Ne plus avoir d'éléctricité",
        "Ne plus avoir d'eau"
    ),
    new Question(
        6,
        "Tu préfèrerais :",
        "assets/victime.gif",
        "Être victime d'une erreur judiciaire",
        "Être victime d'une erreur médicale"
    ),
    new Question(
        7,
        "Tu préfèrerais :",
        "assets/emmerde.gif",
        "Vivre une vie plein d'emmerde",
        "Vivre une vie à t'emmerder"
    ),
    new Question(
        8,
        "Tu préfèrerais :",
        "assets/journuit.gif",
        "Qu'il fasse éternellement jour",
        "Qu'il fasse éternellement nuit"
    ),
    new Question(
        9,
        "Tu préfèrerais :",
        "assets/money.gif",
        "Faire un don d'organe",
        "Faire un don de ta fortune"
    ),
    new Question(
        10,
        "Que dirais-tu si Livio te demandait d'être ton filleul ?",
        "assets/livio.png",
        "Bah oui hein !",
        "Non, j'ai déjà donné !"
    ),
  ])];

class Quizz {
  num id;
  String name;
  String description;
  String image;
  List<Question> questions;

  Quizz(this.id, this.name, this.description, this.image, this.questions);
}

class Question {
  num id;
  String question;
  String image;
  String a1;
  String a2;
  Question(
      this.id, this.question, this.image, this.a1,this.a2);
}


