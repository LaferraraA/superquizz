import 'package:firebase_auth/firebase_auth.dart';

abstract class BaseAuth{
  Future<String> signIn(String email, String password);
  Future<bool> signInValidateUser(String email, String password);
  Future<String> signUp(String email, String password);
  Future<FirebaseUser> getCurrentUser();
  Future<void> signOut();
}

class Auth implements BaseAuth{
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  FirebaseUser firebaseUser;

  @override
  Future<FirebaseUser> getCurrentUser() async{
      firebaseUser = await _firebaseAuth.currentUser();
      return firebaseUser;
  }

  @override
  Future<String> signIn(String email, String password) async{
    firebaseUser = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
    return firebaseUser.uid;
  }

  @override
  Future<bool> signInValidateUser(String email, String password) async{
    firebaseUser = await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
  print("Sign in = ${firebaseUser.uid} , isEmailVerified = ${firebaseUser.isEmailVerified}");
    return firebaseUser.isEmailVerified;
  }
  @override
  Future<void> signOut() async{
    return _firebaseAuth.signOut();
  }

  @override
  Future<String> signUp(String email, String password) async{
    firebaseUser = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
    firebaseUser.sendEmailVerification();
    return firebaseUser.uid;
  }



}